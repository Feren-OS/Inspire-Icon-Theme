Moved to https://github.com/feren-OS/Inspire-Icon-Theme

# Inspire-Icon-Theme
The Official feren OS Icon Theme, available in Default Brown and Blue

Credit: Thubault Clm on YouTube for designing the beginning of the icon set, which then was the style used when I was completing the set, as well as the core feren OS Desktop Design in general.


NOTE: Blue depends on Brown for the core icon set, excluding the folders, and is designed to be installed in a way so that both icon sets are in /usr/share/icons/ under their original names
